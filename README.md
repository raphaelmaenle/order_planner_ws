# Install Requirements
- Ubuntu 20.04
- [ROS 2 Foxy Fitzroy](https://docs.ros.org/en/foxy/Installation.html)
- colcon `sudo apt-get install python3-colcon-common-extensions`
- yaml-cpp `sudo apt-get install libyaml-cpp-dev`

# Pull Instructions

```{bash}
git clone --recurse-submodules https://gitlab.com/raphaelmaenle/order_planner_ws
cd order_planner_ws
```
forgot to pass `--recurse-submodules`?

```{bash}
cd order_planner_ws
git submodule init
git submodule update
```


# Setup Instructions

```{bash}
cd order_planner_ws
colcon build
colcon test
```

# Usage Instructions

- start the node

```{bash}
cd order_planner_ws
. install/setup.bash
ros2 run order_path_planner order_path_planner /path/to/your/file/root
```
or, alternatively, don't pass a path. The path will then be the test data set.

- publish a new order in another terminal
```{bash}
cd order_planner_ws
. install/setup.bash
ros2 topic pub --once /nextOrder order_inferfaces/msg/Order "{order_id:1000001, description: my_order}"
```
in the first terminal you'll now see the order output.

- open rviz2
```{bash}
rviz2
```

and visualize the MarkerArray. 

Note, You'll have to zoom out quite a bit to see the markers. The Distance between the parts can be quite large.


## Docker Setup


```{bash}
docker run althack/ros2:foxy-dev bash
```

in docker container


```{bash}
source /opt/ros/foxy/setup.sh
cd /home/ros
```
